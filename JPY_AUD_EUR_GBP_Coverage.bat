

REM JPY_AUD_EUR_GBP_Coverage

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a tidy -s O:\Prodman\Dev\WFI\Feeds\CntryCurCoverage\Output -t zip -logfile JPY_AUD_EUR_GBP_Coverage

O:\AUTO\Scripts\vbs\FixedIncome\Prod.vbs a Prices_DB O:\Prodman\Dev\WFI\Scripts\Mysql\CntryCur_Coverage\JP_AUD_EUR_GBP_Coverage_Ref.sql O:\Prodman\Dev\WFI\Feeds\CntryCurCoverage\Output\PreLDateSuf.txt :_JPY_AUD_EUR_GBP_Coverage_Ref JPY_AUD_EUR_GBP_Coverage_Ref

O:\AUTO\Scripts\vbs\FixedIncome\newcopyrenamefileAL.vbs O:\Datafeed\Debt\902\ YYMMDD .902 O:\Prodman\Dev\WFI\Feeds\CntryCurCoverage\Output\ YYYYMMDD LOOKUP.txt

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a val -s O:\Prodman\Dev\WFI\Feeds\CntryCurCoverage\Output\ -t txt -n 2 -logfile JPY_AUD_EUR_GBP_Coverage -alert y

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a zip -s O:\Prodman\Dev\WFI\Feeds\CntryCurCoverage\Output\ -d YYYYMMDD_JPY_AUD_EUR_GBP_Coverage -t txt -logfile JPY_AUD_EUR_GBP_Coverage

O:\AUTO\Scripts\vbs\FixedIncome\ftp.vbs O:\Prodman\Dev\WFI\Feeds\CntryCurCoverage\Output\ /Bespoke/CntryCur_Coverage/ .zip both Upload JPY_AUD_EUR_GBP_Coverage
